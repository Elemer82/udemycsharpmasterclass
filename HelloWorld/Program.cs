﻿using HelloWorld.Section_7_Arrays_Lists;
using HelloWorld.Section_8_Debugging;
using HelloWorld.Section_9_Inheritance;
using HelloWorld.Section_10_PolymorphismAndEvenMoreOnOOP_TextFiles;
using HelloWorld.Section_11_AdvancedCSharpTopics;
using Microsoft.VisualBasic;
using System;
using System.ComponentModel;
using System.Transactions;

namespace HelloWorld
{
    //Class names like ClientActivity
    class Program
    {
        //Method name like CalculateValue
        // Method arguments like firstNumber
        
        static void Main(string[] args)
        {
            // 86. Challenge Tic Tac Toe
            // Challenge86_Tic_Tac_Toe.TicTacToe();
            // 87. Jagged Arrays
            // Video87_Jagged_Arrays.JaggedArrays();
            // 88. Challenge jagged Arrays
            // Video88_Challenge_Jagged_Arrays.ChallengeJaggedArrays();
            // 89. Using Arrays as Parameters
            // Video89_Using_Arrays_as_Parameters.UsingArraysasParameters();
            // 91. ArrayLists
            // Video91_ArrayLists.ArrayLists();
            // 93. ArrayLists vs Lists vs Arrays
            // Video93_ArrayLists_vs_Lists_vs_Arrays.ArrayLists_vs_Lists_vs_Arrays();
            // 96. Debugging basics
            // Video96_Debugging_Basics.DebuggingBasics();
            // 97. Locals and autos
            // Video97_Locals_and_Autos.LocalsAndAutos();
            // 98. Debugging, creating copies of Lists and solving some bugs
            // Video98_Debugging_Creating_Copies_of_Lists_and_Solving_Some_Bugs.DebuggingCreatingCopiesOfListsAndSolvingSomeBugs();
            // 99. Debugging Call Stack , Throwing Errors and defensice programming
            // Video99_Debugging_Call_Stack_Throwing_Errors_And_Defensive_Programming.DebuggingCallStackThrowingErrorsAndDefensiveProgramming();
            // 102. Inheritance Demo
            // Video102_Inheritance_Demo.InheritanceDemo();
            // 103. Inheritance Challenge - Videopost and Timer with Callback
            // Video103_Inheritance_Challenge.VideopostAndTimerWithCallback();
            // 104. Inheritance Challenge 2 - Employees, Bosses and Trainees
            // Video104_Inheritance_Challenge2.EmployeesBossesAndTrainees();
            // 105.Inheritance Challenge 2 - Employees,Bosses and Trainees Solution
            // Video105_Inheritance_Challenge2_Solution.EmployeesBossesAndTraineesSolution();
            // 106. Interfaces
            // Video106_Interfaces.Video106_Main();
            // 109. Polymorphic Parameters
            // Video109_PolymorphicParameters.Video109_Main();
            // 110. Sealed Key Word
            // Video110_SealedKeyWord.Video110_Main();
            // 111. Has A - Relationships
            // Video111__Has_A_Relationships.Video111_Main();
            // 112. Read from a Textfile
            // Video112_ReadFromATextFile.Video112_Main();
            // 113. Write into a Text File
            // Video113_WriteIntoATextFile.Video113_Main();
            // 118. Structs
            // Video118_Structs.Video118_Main();
            // 119. Enums
            // Video119_Enums.Video119_Main();
            // 120. Math Class
            // Video120_MathClass.Video120_Main();
            // 121. Random Class
            // Video121_RandomClass.Video121_Main();
            // 123. Regular Expressions
            // Video123_RegularExpressions.Video123_Main();
            // 124. DateTime
            Video124_DateTime.Video124_Main();


            Console.Read();
            
        }

        //<access modifier> (static) <return type> <method name> (parameter1, parameter2)
        
    }
}
