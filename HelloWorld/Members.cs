﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace HelloWorld
{
    class Members
    {
        // member - private field
        private string memberName;
        private string jobTitle;
        private int salary = 20000;

        // member - public field
        public int age;

        // member - property - exposes jobTitle safely - properties start with a capital letter
        public string JobTitle {
            get {
                return this.jobTitle;
            }
            set {
                this.jobTitle = value;
            }
        }

        // public member Methos - can be called from other classes
        public void Introducing(bool isFriend) {
            if (isFriend)
            {
                SharingPrivateInfo();
            }
            else
            {
                Console.WriteLine("Hi, my name is {0} and my job title is {1} and I', {2} years old.", memberName, JobTitle ,age);
            }
        }

        private void SharingPrivateInfo() {
            Console.WriteLine("My salary is {0}", salary);
        }

        //meber constructor
        public Members() {
            age = 30;
            memberName = "Lucy";
            salary = 60000;
            jobTitle = "Developer";
            Console.WriteLine("Object created.");
        }


        // member - finalizer - destructor
        // Should only be used if there is something useful to be done here. Performance issue otherwise
        ~Members() {
            //cleanup statements
            Console.WriteLine("DeStruction of member object");
            Debug.Write("Destruction of member object");

        }
    }
}
