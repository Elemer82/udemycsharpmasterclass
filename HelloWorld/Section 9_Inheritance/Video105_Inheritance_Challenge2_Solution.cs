﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HelloWorld.Section_9_Inheritance
{
    class Video105_Inheritance_Challenge2_Solution
    {
        public static void EmployeesBossesAndTraineesSolution() {

            Employee105 michael = new Employee105("Michael","Miller",40000);
            michael.Work();
            michael.Pause();
            Console.WriteLine();

            Boss105 chuckNorris = new Boss105("Ferrari", "Chuck", "Norris", 100000);
            chuckNorris.Lead();
            chuckNorris.Work();
            chuckNorris.Pause();
            Console.WriteLine();

            Trainee105 michelle = new Trainee105(32, 8, "Michelle", "Gartner", 10000);
            michelle.Learn();
            michelle.Work();
            Console.WriteLine();


            Console.ReadKey();
        }
    }

    class Employee105
    {
        public string Name { get; set; }
        public string FirstName { get; set; }
        public int Salary { get; set; }
        public Employee105() {
            FirstName = "Elemer";
            Name = "Gazda";
            Salary = 50000;
        }

        public Employee105(string firstName, string name, int salary)
        {
            this.Name = name;
            this.FirstName = firstName;
            this.Salary = salary;
        }
        public void Work()
        {
            Console.WriteLine("Employee {0} {1} working", this.FirstName, this.Name);
        }

        public void Pause()
        {
            Console.WriteLine("Employee {0} {1} Pause", this.FirstName, this.Name);
        }


    }

    class Boss105:Employee105
    {
        public string CompanyCar { get; set; }
        public Boss105(string companyCar, string name, string firstName, int salary) : base(name, firstName, salary) {
            this.CompanyCar = companyCar;
        }
        public void Lead()
        {
            Console.WriteLine("Boss {0} {1} Leads", this.FirstName, this.Name);
        }
    }

    class Trainee105:Employee105
    {
        public int WorkingHours { get; set; }
        public int SchoolHours { get; set; }
        public Trainee105() { }
        public Trainee105(int workingHours, int schoolHours, string name, string firstName,int salary):base(firstName,name,salary)
        {
            this.WorkingHours = workingHours;
            this.SchoolHours = schoolHours;
        }
        public void Learn()
        {
            Console.WriteLine("Trainee {0} {1} Learns for {2} hours", this.FirstName, this.Name, this.SchoolHours);
        }

        public void Work()
        {
            Console.WriteLine("Working hours for {0} {1} is {2} hours", this.FirstName, this.Name, this.WorkingHours);
        }
    }
}
