﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace HelloWorld.Section_9_Inheritance
{
    class Video104_Inheritance_Challenge2
    {
        public static void EmployeesBossesAndTrainees()
        {
            Employee employee1 = new Employee("David","Gazda");
            Boss boss1 = new Boss("Eniko","Gazda");
            Trainees trainees1 = new Trainees("Elemer", "Gazda");

            employee1.Work();
            employee1.Pause();
            Console.WriteLine();

            boss1.Lead();
            boss1.Work();
            boss1.Pause();
            Console.WriteLine();

            trainees1.Work();
            trainees1.Pause();
            trainees1.Learn();
        }

    }
    class Employee
    {
        protected string Name { get; set; }
        protected string FirstName { get; set; }
        protected int Salary { get; set; }
        public Employee() { }

        public Employee(string name, string firstName) {
            this.Name = name;
            this.FirstName = firstName;
        }
        public virtual void Work() {
            Console.WriteLine("Employee {0} {1} working",this.FirstName, this.Name);
        }

        public void Pause() {
            Console.WriteLine("Employee {0} {1} Pause",this.FirstName,this.Name);
        }


    }

    class Boss:Employee { 
        protected string CompanyCar { get; set; }
        public Boss() { }
        public Boss(string name, string firstName)
        {
            this.Name = name;
            this.FirstName = firstName;
        }
        public void Lead() {
            Console.WriteLine("Boss {0} {1} Leads", this.FirstName,this.Name);
        }
    }

    class Trainees : Employee { 
        protected int WorkingHours { get; set; }
        protected int SchoolHours { get; set; }
        public Trainees() { }
        public Trainees(string name, string firstName)
        {
            this.Name = name;
            this.FirstName = firstName;
        }
        public void Learn() {
            Console.WriteLine("Trainee {0} {1} Learns", this.FirstName, this.Name);
        }

        public override void Work() {
            Console.WriteLine("Working hours for {0} {1} are {2}",this.FirstName, this.Name, this.WorkingHours);
        }
    }

}
