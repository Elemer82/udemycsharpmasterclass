﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;

namespace HelloWorld.Section_9_Inheritance
{
    class Video103_Inheritance_Challenge
    {
        public static void VideopostAndTimerWithCallback()
        {
            VideoPost videoPost1 = new VideoPost("My first video post", "Elemer Gazda", "https://videos.com/firstvideo", 5, true);
            Console.WriteLine(videoPost1.ToString());
            
            videoPost1.Play();
            Console.WriteLine("Press any key to stop the video");
            Console.ReadKey();
            videoPost1.Stop();
            
            Console.Read();
        }
    }

    class VideoPost : Post
    {
        //member fields
        protected bool isPlaying = false;
        protected int currDuration = 0;
        private static Timer timer = null;

        //Properties
        protected string VideoURL { get; set; }
        protected int Length { get; set; }
        
        private int PlayerSeconds { get; set; }
        public VideoPost() { }
        public VideoPost(string title, string sendByUsername, string videoURL, int length, bool isPublic)
        {
            // The following properties and the GetNextID method are inherited from Post.
            this.ID = GetNextID();
            this.Title = title;
            this.SendByUsername = sendByUsername;
            this.IsPublic = isPublic;
            // Property VideoURL is a member of VideoPost, but not of Post
            this.VideoURL = videoURL;
            this.Length = length;
        }

        public override string ToString()
        {
            return String.Format("{0} - {1} - with URL {2} - {3} seconds - by {4}", 
                this.ID, this.Title, this.VideoURL, this.Length, this.SendByUsername);
        }

        private void EleTimerCallback(object state)
        {
            Console.WriteLine("Currently playing video at second {0}", this.PlayerSeconds);
            this.PlayerSeconds++;
            //Console.WriteLine(Thread.CurrentThread.ManagedThreadId.ToString());
            Thread.Sleep(1000);
        }

        public void ElePlay()
        {
            Console.WriteLine(Environment.NewLine + "Starting the Timer" + Environment.NewLine);
            //Sample 04: Create and Start The Timer
            this.PlayerSeconds = 0;
            timer = new Timer(new TimerCallback(EleTimerCallback), null, 0, 1000);
            EleStop();
        }

        public void EleStop()
        {
            while (true)
            {
                ConsoleKeyInfo key = Console.ReadKey();
                
                if (key != null)
                {
                    if (timer == null)
                    {
                        Console.WriteLine(Environment.NewLine + "Timer Not " + "Yet Started" + Environment.NewLine);
                        continue;
                    }
                    Console.WriteLine(Environment.NewLine + "Stopping the Timer" + Environment.NewLine);
                    //Sample 05: Stop The Timer
                    timer.Change(Timeout.Infinite, Timeout.Infinite);
                    timer.Dispose();
                    break;
                }
                
            }

        }

        public void Play() {
            if (!isPlaying)
            {
                Console.WriteLine("Playing");
                timer = new Timer(TimerCallback, null, 0, 1000);
                isPlaying = true;
            }
        }

        private void TimerCallback(object o) {
            if (currDuration <= Length)
            {
                Console.WriteLine("Video at {0}", currDuration++);
                GC.Collect();
            }
            else
            {
                Stop();  
            }
        }
        public void Stop() {
            if (isPlaying)
            {
                Console.WriteLine("Stopped at {0}", currDuration);
                currDuration = 0;
                timer.Dispose();
                isPlaying = false;
            }
        }
    }

}
