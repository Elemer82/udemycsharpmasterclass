﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace HelloWorld.Section_9_Inheritance
{
    class Video106_Interfaces
    {
        public static void Video106_Main() {
            Notification n1 = new Notification("Elemer","Tsup bro?","12.06.2018-18.07.2020");
            Notification n2 = new Notification("Frank","All good buddy", "12.06.2018-18.07.2020");

            n1.ShowNotification();
            n2.ShowNotification();

            Console.ReadKey();
        }

    }

    public interface INotifications {
        void ShowNotification();
        string getDate();
    }

    public class Notification:INotifications
    {
        private string sender;
        private string message;
        private string date;

        // default constructor
        public Notification() {
            sender = "admin";
            message = "Yo, whats up?";
            date = "";
        }
        public Notification(string mySender, string myMessage, string myDate) {
            this.sender = mySender;
            this.message = myMessage;
            this.date = myDate;
        }
        public void ShowNotification() {
            Console.WriteLine("Message {0} - was sent by {1} - at {2}",message, sender,date);
        }
        public string getDate() {
            return date;
        }
    }
}
