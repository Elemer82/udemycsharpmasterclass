﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace HelloWorld.Section_9_Inheritance
{
    class Video102_Inheritance_Demo
    {
        public static void InheritanceDemo() {
            Post post1 = new Post("Thanks for the birthday wishes", true, "Elemer Gazda");
            Console.WriteLine(post1.ToString());

            ImagePost imagePost1 = new ImagePost("Check out my new shoes","Elemer Gazda","https://images.com/shoes",true);
            Console.WriteLine(imagePost1.ToString());  // To.String is inherited from Post
            Console.Read();
        }

    }

    class Post
    {
        private static int currentPostId;

        // properties
        protected int ID { get; set; }
        protected string Title { get; set; }
        protected string SendByUsername { get; set; }
        protected bool IsPublic { get; set; }

        // Default Constructor. If a derived class does not invoke a base-class
        // constructor explivitly,
        // the default constructor is called implicitly
        public Post() {
            ID = 0;
            Title = "My First Post";
            IsPublic = true;
            SendByUsername = "Elemer Gazda";
        }

        // Instance Constructor that has three parameters
        public Post(string title, bool isPublic, string sendByUsername) {
            this.ID = GetNextID() ;
            this.Title = title;
            this.SendByUsername = sendByUsername;
            this.IsPublic = isPublic;
        }

        protected int GetNextID() {
            return ++currentPostId;
        }

        public void Update(string title, bool isPublic) {
            this.Title = title;
            this.IsPublic = isPublic;
        }

        // Virtual method override of the ToString method is inherited
        // from System.Object
        public override string ToString()
        {
            return String.Format("{0} - {1} - by {2}", this.ID, this.Title, this.SendByUsername);
        }
    }

    // ImagePost derives from Post and adds a property (ImageURL) and two constructors
    class ImagePost:Post { 
        public string ImageURL { get; set; }

        public ImagePost() { }
        public ImagePost(string title, string sendByUsername, string imageURL, bool isPublic) {
            // The following properties and the GetNextID method are inherited from Post.
            this.ID = GetNextID();
            this.Title = title;
            this.SendByUsername = sendByUsername;
            this.IsPublic = isPublic;
            // Property ImageURL is a member of ImagePost, but not of Post
            this.ImageURL = imageURL;
        }

        public override string ToString()
        {
            return String.Format("{0} - {1} - with URL {2} - by {3}", this.ID, this.Title, this.ImageURL, this.SendByUsername);
        }

    }

    }

