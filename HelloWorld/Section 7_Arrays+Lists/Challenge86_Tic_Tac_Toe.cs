﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HelloWorld.Section_7_Arrays_Lists
{
    class Challenge86_Tic_Tac_Toe
    {
        static bool weHaveAWinner = false;
        static int player = 2; // Player 1 starts
        static int turns = 0;
        static string[,] grid = new string[3, 3]
            {
                    {"1", "2", "3" },
                    {"4", "5", "6" },
                    {"7", "8", "9" }
            };
        static string[] playersigns = { "X", "O" };
        public static void TicTacToe() {
            PrintGrid(grid);
            do
            {
                
                player = player == 2 ? 1 : 2;
                
                PromptPlayer(player,grid, playersigns[player-1]);
                PrintGrid(grid);
                weHaveAWinner = IsWinner(grid, player);
                if (weHaveAWinner)
                {
                    PrintGrid(grid);
                    Console.WriteLine("Congratulations Player {0} ({1}), you won !", player.ToString(),playersigns[player-1]);
                    Console.WriteLine("Press any Key to Reset the Game");
                    Console.ReadKey();
                    ResetField();
                }
                else if (turns == 10)
                {
                    Console.WriteLine("Draw");
                    Console.WriteLine("Press any Key to Reset the Game");
                    Console.ReadKey();
                    ResetField();
                }
            } while (true);
        }

        private static void PrintGrid(string[,] array2D) {
            Console.Clear();
            Console.WriteLine("-------------");
            for (int i = 0; i < array2D.GetLength(0); i++)
            {
                Console.WriteLine("|   |   |   |");
                Console.Write("|");
                for (int j = 0; j < array2D.GetLength(1); j++)
                {
                    Console.Write(" {0} |", array2D[i,j]);
                }
                Console.WriteLine();
                Console.WriteLine("|   |   |   |");
                Console.WriteLine("-------------");
            }
            turns++;
        }

        private static string PromptPlayer(int player, string[,] array2D, string sign) {
            string input = string.Empty;
            int inputInteger;
            do
            {
                Console.WriteLine("Player {0}, your character is {1}.", player, playersigns[player-1]);
                Console.WriteLine("Choose your field!");
                input = Console.ReadLine();

                if (int.TryParse(input, out inputInteger))
                {
                    bool isOptionAvailable = IsOptionAvailable(array2D, inputInteger, sign, out array2D);
                    if (!isOptionAvailable)
                    {
                        Console.WriteLine("   That option is not a valid option, please try again");
                        input = String.Empty;
                    }
                }
                else
                {
                    Console.WriteLine("   Invalid input {0}" , input);
                    Console.WriteLine("   {0} is not an integer ", input);
                    input = String.Empty;
                }

            } while (input == string.Empty);
            return input;
        }

        private static bool IsOptionAvailable(string[,] array2D, int inputInteger, string sign, out string[,] array2DNew) {
            for (int i = 0; i < array2D.GetLength(0); i++)
            {
                for (int j = 0; j < array2D.GetLength(1); j++)
                {
                    if (array2D[i,j] == inputInteger.ToString())
                    {
                        array2D[i, j] = sign;
                        array2DNew = array2D;
                        return true;
                    }
                }
            }
            array2DNew = array2D;
            return false;
        }

        private static bool IsWinner(string[,] array2D, int player) {
            bool isWinner = false;
            
            foreach (string playersign in playersigns)
            {
                if (   ((array2D[0, 0] == playersign) && (array2D[0, 1] == playersign) && (array2D[0, 2] == playersign))
                    || ((array2D[1, 0] == playersign) && (array2D[1, 1] == playersign) && (array2D[1, 2] == playersign))
                    || ((array2D[2, 0] == playersign) && (array2D[2, 1] == playersign) && (array2D[2, 2] == playersign))

                    || ((array2D[0, 0] == playersign) && (array2D[1, 0] == playersign) && (array2D[2, 0] == playersign))
                    || ((array2D[0, 1] == playersign) && (array2D[1, 1] == playersign) && (array2D[2, 1] == playersign))
                    || ((array2D[0, 2] == playersign) && (array2D[1, 2] == playersign) && (array2D[2, 2] == playersign))

                    || ((array2D[0, 0] == playersign) && (array2D[1, 1] == playersign) && (array2D[2, 2] == playersign))
                    || ((array2D[0, 2] == playersign) && (array2D[1, 1] == playersign) && (array2D[2, 0] == playersign))
                )
                {
                    Console.WriteLine("We have a winner");
                    Console.WriteLine("Player {0} won !", player.ToString());
                    isWinner = true;
                }
            }
            return isWinner;
        }

        private static void ResetField() {
            grid = new string[3, 3]
            {
                    {"1", "2", "3" },
                    {"4", "5", "6" },
                    {"7", "8", "9" }
            };
            PrintGrid(grid);
            turns = 0;
        }
    }
}
