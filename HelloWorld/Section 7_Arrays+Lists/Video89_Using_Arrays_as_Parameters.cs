﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Transactions;

namespace HelloWorld.Section_7_Arrays_Lists
{
    class Video89_Using_Arrays_as_Parameters
    {
        public static void UsingArraysasParameters() { 
        

            int[] studentsGrades = new int[] { 15, 13, 8, 12, 6, 16 };
            double averageResult = GetAverage(studentsGrades);

            foreach (int grade in studentsGrades)
            {
                Console.WriteLine("{0}", grade);
            }
            
            Console.WriteLine("The average is {0}", averageResult);

            int[] originalHappiness = new int[] {1,3,5,6,7 };
            int[] newHappiness = IncreaseHappiness(originalHappiness);
            foreach (int happiness in newHappiness)
            {
                Console.WriteLine("{0}", happiness);
            }


        }

        static double GetAverage(int[] grades) {
            int size = grades.Length;
            double average;
            int sum = 0;
            for (int i = 0; i < size; i++)
            {
                sum += grades[i];
            }
            average = (double)sum / size;
            return average;
        }

        static int[] IncreaseHappiness(int[] happinessArray) {
            int size = happinessArray.Length;
            int[] resultArray = new int[size];
            for (int i = 0; i < size; i++)
            {
                resultArray[i] = happinessArray[i] + 2;
            }

            return resultArray;
        }
    }
}
