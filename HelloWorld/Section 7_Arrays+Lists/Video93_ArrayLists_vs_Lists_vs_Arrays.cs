﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace HelloWorld.Section_7_Arrays_Lists
{
    class Video93_ArrayLists_vs_Lists_vs_Arrays
    {

        public static void ArrayLists_vs_Lists_vs_Arrays() {

            // immutable - limited to one type
            int[] scores = new int[] { 99, 96, 87, 76 };

            // Lists , are mutable (extendable and shrinkable) - still limited to one data type
            List<int> list = new List<int> { 1, 2, 3, 4 };
            list.Add(0); // ads an object to the end of the list
            list.Add(32);
            list.Sort();
            list.RemoveRange(2, 2);

            foreach (int i in list)
            {
                Console.WriteLine(i);
            }

            Console.WriteLine(list.Contains(4));

            int index = list.FindIndex(x=> x == 4); // used lambda expression, not sure what this is though
            Console.WriteLine(list[index]);

            list.RemoveAt(index);

            list.ForEach(i => Console.WriteLine(i)); // another lambda expression to list all elements

            // Arraylists - are mutable (extendable and shrinkable)
            //can contain different type of element (string + int + char + double + etc.)
            
            ArrayList arrayList = new ArrayList();
            arrayList.Add(1);
            arrayList.Add("two");
            arrayList.Add("three");
            arrayList.Add(new Number { n = 4 });

            foreach (object o in arrayList)
            {
                Console.WriteLine(o);
            }

            Console.Read();
        }
    }
    class Number { 
        public int n { get; set; }
        public override string ToString()
        {
            return n.ToString();
        }
    }
}
