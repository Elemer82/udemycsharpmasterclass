﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Text;

namespace HelloWorld.Section_7_Arrays_Lists
{
    class Video87_Jagged_Arrays
    {

        public static void JaggedArrays() {

            //declare jagged array
            int[][] jaggedArray = new int[3][];

            jaggedArray[0] = new int[5];
            jaggedArray[1] = new int[3];
            jaggedArray[2] = new int[2];

            jaggedArray[0] = new int[] { 2, 3, 5, 7, 11 };
            jaggedArray[1] = new int[] { 1, 2, 3};
            jaggedArray[2] = new int[] { 13, 21 };

            //alternative way:
            int[][] jaggedArray2 = new int[][] {
                 new int[] { 2, 3, 5, 7, 11 },
                 new int[] { 1, 2, 3},
                 new int[] { 13, 21 }
            };

            Console.WriteLine("The value in the middle of the frist entry is {0}", jaggedArray2[0][2]);
            ListJaggedArrayItems(jaggedArray2);
            Console.Read();
        }

        private static void ListJaggedArrayItems(int[][] jaggedArraytoList) {
            Console.WriteLine( "Array Rank is {0}", jaggedArraytoList.Rank );
            Console.WriteLine("Array length is {0}", jaggedArraytoList.Length);
            for (int i = 0; i < jaggedArraytoList.Length; i++)
            {
                Console.WriteLine("Element {0}", i);
                for (int j = 0; j < jaggedArraytoList[i].Length; j++)
                {
                    Console.WriteLine("Array {0}, element {1} is {2}", i, j , jaggedArraytoList[i][j]);
                }
            }
        }
    }
}
