﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HelloWorld.Section_7_Arrays_Lists
{
    class Video88_Challenge_Jagged_Arrays
    {
        public static void ChallengeJaggedArrays() {
            string[][] friendsArrays = new string[3][];
            friendsArrays[0] = new string[] { "Joe", "Fred" };
            friendsArrays[1] = new string[] { "Milka", "Charlie" };
            friendsArrays[2] = new string[] { "George", "Scott" };

            for (int i = 0; i < friendsArrays.Length; i++)
            {
                for (int j = 0; j < friendsArrays[i].Length; j++)
                {

                    for (int k = 0; k < friendsArrays.Length; k++)
                    {
                        for (int l = 0; l < friendsArrays[k].Length; l++)
                        {
                            if (i != k)
                            {
                                Console.WriteLine("Hi {0}, I am {1}.", friendsArrays[k][l], friendsArrays[i][j]);
                            }
                            else {
                                //Console.WriteLine("{0} is in the same family as {1}.", friendsArrays[k][l], friendsArrays[i][j]);
                            }
                        }
                    }

                }
            }

        }
    }
}
