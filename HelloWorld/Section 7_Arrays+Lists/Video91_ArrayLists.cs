﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace HelloWorld.Section_7_Arrays_Lists
{
    class Video91_ArrayLists
    {
        public static void ArrayLists() {

            //declaring an Arraylist with UNdefined amount of objects
            ArrayList myArrayList = new ArrayList();
            //declaring an Arraylist with Defined amount of objects
            ArrayList myArrayList2 = new ArrayList(100);

            myArrayList.Add(25);
            myArrayList.Add("Hello");
            myArrayList.Add(13.37);
            myArrayList.Add(13);
            myArrayList.Add(128);
            myArrayList.Add(25.3);
            myArrayList.Add(13);

            // This will delete element with specific value
            // first occurence will be removed.
            myArrayList.Remove(13);

            // delete element at specific position.
            myArrayList.RemoveAt(0);

            Console.WriteLine(myArrayList.Count);

            double sum = 0;
            foreach (object obj in myArrayList)
            {
                if (obj is int)
                {
                    sum += Convert.ToDouble(obj);
                }
                else if (obj is double)
                {
                    sum += (double)obj;
                }
                else if (obj is string)
                {
                    Console.WriteLine(obj);
                }
            }

            Console.WriteLine("Sum is {0}",sum);

        }


    }
}
