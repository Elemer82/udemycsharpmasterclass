﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HelloWorld.Section_10_PolymorphismAndEvenMoreOnOOP_TextFiles
{
    class Video110_SealedKeyWord
    {
        public static void Video110_Main()
        {
            Video110_M3 myM3 = new Video110_M3(260, "red", "M3 Super Turbo");
            myM3.Repair();

            Console.Read();
        }
    }

    class Video110_Car
    {

        public int HP { get; set; }
        public string Color { get; set; }

        public Video110_Car(int hp, string color)
        {
            this.HP = hp;
            this.Color = color;
        }

        public void ShowDetails()
        {
            Console.WriteLine("This car is {0} and it has {1} HP", this.Color, this.HP);
        }

        public virtual void Repair()
        {
            Console.WriteLine("Car was repaired!");
        }
    }
    class Video110_Audi : Video110_Car
    {
        private string brand = "Audi";
        public string Model { get; set; }
        public Video110_Audi(int hp, string color, string model) : base(hp, color)
        {
            this.Model = model;
        }
        public void ShowDetails()
        {
            Console.WriteLine("This is a {0} model {1} of color {2} and it has {3} HP", this.brand, this.Model, this.Color, this.HP);
        }

        public override void Repair()
        {
            Console.WriteLine("The {0} {1} was repaired!", this.brand, this.Model);
        }
    }
    class Video110_BMW : Video110_Car
    {
        private string brand = "BMW";
        public string Model { get; set; }
        public Video110_BMW(int hp, string color, string model) : base(hp, color)
        {
            this.Model = model;
        }

        // new keyword does not override the inherited ShowDetails() but reprioritises
        // Video110_Car bmwZ3 = new Video110_BMW(200, "black", "Z3"); -- In this case ShowDetails is used from Cars
        // Video110_BMW bmwM5 = new Video110_BMW(330, "white", "M5"); -- In this case ShowDetails is used from BMW because of new keyword
        public new void ShowDetails()
        {
            Console.WriteLine("This is a {0} model {1} of color {2} and it has {3} HP", this.brand, this.Model, this.Color, this.HP);
        }

        public sealed override void Repair()
        {
            Console.WriteLine("The {0} {1} was repaired!", this.brand, this.Model);
        }
    }

    class Video110_M3 : Video110_BMW
    {

        public Video110_M3(int hp, string color, string model) : base(hp, color, model)
        {

        }

        /* Cannot ovveride because Repair is sealed at BMW Level
         * public override void Repair()
        {
            base.Repair();
        }
        */
    }
}