﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Xml.Schema;

namespace HelloWorld.Section_10_PolymorphismAndEvenMoreOnOOP_TextFiles
{
    class Video109_PolymorphicParameters
    {
        public static void Video109_Main() {

            // POlymorphism at work #1 :
            // An Audi, BMW, Porsche can all be used whereever a Car is expected.
            // No cast is required because an imlicit conversion exists from a derived class to its base class
            var cars = new List<Video109_Car>
            {
                new Video109_Audi(200, "blue", "A4"),
                new Video109_BMW(250, "red", "M3")
            };

            // Plymorphism at work #2 :
            // The virtual method Repair is invoked on each of the derived classes, not the base class.
            foreach (var car in cars) {
                car.Repair();            
            }

            Video109_Car bmwZ3 = new Video109_BMW(200, "black", "Z3");
            Video109_Car audiA3 = new Video109_Audi(100, "green", "A3");
            Video109_BMW bmwM5 = new Video109_BMW(330, "white", "M5");

            bmwZ3.ShowDetails();
            audiA3.ShowDetails();
            bmwM5.ShowDetails();

            Video109_Car carB = (Video109_Car)bmwM5;
            carB.ShowDetails();

            Console.Read();
        
        }
    }

    class Video109_Car { 
    
        public int HP { get; set; }
        public string Color { get; set; }

        public Video109_Car(int hp, string color) {
            this.HP = hp;
            this.Color = color;
        }

        public void ShowDetails() {
            Console.WriteLine("This car is {0} and it has {1} HP", this.Color, this.HP);
        }

        public virtual void Repair() {
            Console.WriteLine("Car was repaired!");
        }
    }
    class Video109_Audi:Video109_Car
    {
        private string brand = "Audi";
        public string Model { get; set; }
        public Video109_Audi(int hp, string color, string model):base(hp,color){
            this.Model = model;
        }
        public void ShowDetails()
        {
            Console.WriteLine("This is a {0} model {1} of color {2} and it has {3} HP", this.brand, this.Model, this.Color, this.HP);
        }

        public override void Repair()
        {
            Console.WriteLine("The {0} {1} was repaired!", this.brand, this.Model);
        }
    }
    class Video109_BMW:Video109_Car {
        private string brand = "BMW";
        public string Model { get; set; }
        public Video109_BMW(int hp, string color, string model) : base(hp, color)
        {
            this.Model = model;
        }

        // new keyword does not override the inherited ShowDetails() but reprioritises
        // Video109_Car bmwZ3 = new Video109_BMW(200, "black", "Z3"); -- In this case ShowDetails is used from Cars
        // Video109_BMW bmwM5 = new Video109_BMW(330, "white", "M5"); -- In this case ShowDetails is used from BMW because of new keyword
        public new void ShowDetails()
        {
            Console.WriteLine("This is a {0} model {1} of color {2} and it has {3} HP", this.brand, this.Model, this.Color, this.HP);
        }

        public override void Repair()
        {
            Console.WriteLine("The {0} {1} was repaired!", this.brand, this.Model);
        }
    }
}
