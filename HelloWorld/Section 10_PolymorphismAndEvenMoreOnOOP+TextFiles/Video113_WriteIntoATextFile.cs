﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace HelloWorld.Section_10_PolymorphismAndEvenMoreOnOOP_TextFiles
{
    class Video113_WriteIntoATextFile
    {
        public static void Video113_Main() {

            // Method 1
            string[] lines = { "First line 242", "Second line 240", "Third line 199" };
            File.WriteAllLines(@"C:\Programozas\Udemy\ReadFile\testFile2.txt", lines);

            string[] highscores = { "Elemer - 100", "Eniko - 82", "David - 15" };
            File.WriteAllLines(@"C:\Programozas\Udemy\ReadFile\HighScores.txt", highscores);
            File.WriteAllLines(@"C:\Programozas\Udemy\ReadFile\HighScores.txt", highscores); // OverWrites existing file

            // Method 2
            /*
            Console.WriteLine("Specify filename:");
            string filename = Console.ReadLine();
            Console.WriteLine("Please specify the content:");
            string input = Console.ReadLine();

            File.WriteAllText(@"C:\Programozas\Udemy\ReadFile\" + filename + ".txt", input);
            */
            // Method 3
            using (StreamWriter file = new StreamWriter(@"C:\Programozas\Udemy\ReadFile\Method3.txt")) {
                foreach (string line in lines)
                {
                    if (line.Contains("Third"))
                    {
                        file.WriteLine(line);
                    }
                }
            }

            using (StreamWriter file = new StreamWriter(@"C:\Programozas\Udemy\ReadFile\Method3-2.txt"))
            {
                foreach (string line in lines)
                {
                    if (line.Contains("2"))
                    {
                        file.WriteLine(line);
                    }
                }
            }

            using (StreamWriter file = new StreamWriter(@"C:\Programozas\Udemy\ReadFile\Method3-2.txt",true)) // bool true appends
            {
                file.WriteLine("Additional line");
            }

            Console.WriteLine("Done");
            Console.ReadKey();
        }
    }
}
