﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HelloWorld.Section_10_PolymorphismAndEvenMoreOnOOP_TextFiles
{
    class Video112_ReadFromATextFile
    {
        public static void Video112_Main() {
            
            // Example 1 - reading Text
            string text = System.IO.File.ReadAllText(@"C:\Programozas\Udemy\ReadFile\testFile.txt");
            Console.WriteLine("Text file contains :");
            Console.WriteLine(text);

            // Example 2 - Readsing Text
            string[] lines = System.IO.File.ReadAllLines(@"C:\Programozas\Udemy\ReadFile\testFile.txt");
            Console.WriteLine("Text file contains :");

            foreach (string line in lines)
            {
                Console.WriteLine("\t" + line);
            }


            Console.Read();
        }

    }
}
