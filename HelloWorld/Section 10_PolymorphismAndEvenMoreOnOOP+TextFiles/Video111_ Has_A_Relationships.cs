﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HelloWorld.Section_10_PolymorphismAndEvenMoreOnOOP_TextFiles
{
    class Video111__Has_A_Relationships
    {
        public static void Video111_Main() {

            Video111_Car bmwZ3 = new Video111_BMW(200, "black", "Z3");
            Video111_Car audiA3 = new Video111_Audi(100, "green", "A3");
            Video111_BMW bmwM5 = new Video111_BMW(330, "white", "M5");

            bmwZ3.ShowDetails();
            audiA3.ShowDetails();
            bmwM5.ShowDetails();

            bmwZ3.SetCarIDINfo(1234, "Elemer Gazda");
            audiA3.SetCarIDINfo(1235,"Frank White");

            bmwZ3.GetCarIDInfo();
            audiA3.GetCarIDInfo();

            Video111_Car carB = (Video111_Car)bmwM5;
            carB.ShowDetails();

            Console.Read();

        }
    }

    class Video111_Car
    {

        public int HP { get; set; }
        public string Color { get; set; }

        // has a Relationship
        protected Video111_CarIDInfo carIDInfo = new Video111_CarIDInfo();
        public void SetCarIDINfo(int idNum,string owner) {
            carIDInfo.IDNum = idNum;
            carIDInfo.Owner = owner;
        }

        public void GetCarIDInfo() {
            Console.WriteLine("The car has an ID of {0} and is owned by {1}", carIDInfo.IDNum, carIDInfo.Owner);
        }

        public Video111_Car(int hp, string color)
        {
            this.HP = hp;
            this.Color = color;
        }

        public void ShowDetails()
        {
            Console.WriteLine("This car is {0} and it has {1} HP", this.Color, this.HP);
        }

        public virtual void Repair()
        {
            Console.WriteLine("Car was repaired!");
        }
    }
    class Video111_Audi : Video111_Car
    {
        private string brand = "Audi";
        public string Model { get; set; }
        public Video111_Audi(int hp, string color, string model) : base(hp, color)
        {
            this.Model = model;
        }
        public void ShowDetails()
        {
            Console.WriteLine("This is a {0} model {1} of color {2} and it has {3} HP", this.brand, this.Model, this.Color, this.HP);
        }

        public override void Repair()
        {
            Console.WriteLine("The {0} {1} was repaired!", this.brand, this.Model);
        }
    }

    // a BMW is a Car
    class Video111_BMW : Video111_Car
    {
        private string brand = "BMW";
        public string Model { get; set; }
        public Video111_BMW(int hp, string color, string model) : base(hp, color)
        {
            this.Model = model;
        }

        // new keyword does not override the inherited ShowDetails() but reprioritises
        // Video110_Car bmwZ3 = new Video110_BMW(200, "black", "Z3"); -- In this case ShowDetails is used from Cars
        // Video110_BMW bmwM5 = new Video110_BMW(330, "white", "M5"); -- In this case ShowDetails is used from BMW because of new keyword
        public new void ShowDetails()
        {
            Console.WriteLine("This is a {0} model {1} of color {2} and it has {3} HP", this.brand, this.Model, this.Color, this.HP);
        }

        public sealed override void Repair()
        {
            Console.WriteLine("The {0} {1} was repaired!", this.brand, this.Model);
        }
    }

    // an M3 is a BMW
    class Video111_M3 : Video110_BMW
    {

        public Video111_M3(int hp, string color, string model) : base(hp, color, model)
        {

        }

        /* Cannot ovveride because Repair is sealed at BMW Level
         * public override void Repair()
        {
            base.Repair();
        }
        */
    }

    // Has a
    class Video111_CarIDInfo {
        public int IDNum { get; set; } = 0;
        public string Owner { get; set; } = "No owner";

    }
}
