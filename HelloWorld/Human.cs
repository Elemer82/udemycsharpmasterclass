﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HelloWorld
{

    // This class is a blueprint for a datatype
    class Human
    {
        //member variable protected by default
        private string firstName;
        private string lastName;
        private string eyeColor;
        private int age;

        // constructor

        public Human() {
            Console.WriteLine("Constructor caled. Object Created");
        }

        // parameterised Constructor
        public Human(string firstName)
        {
            this.firstName = firstName;
        }

        // parameterised Constructor
        public Human(string firstName, string lastName) {
            this.firstName = firstName;
            this.lastName = lastName;
        }

        // parameterised Constructor
        public Human(string firstName, string lastName, string eyeColor)
        {
            this.firstName = firstName;
            this.lastName = lastName;
            this.eyeColor = eyeColor;
        }

        // parameterised Constructor
        public Human(string firstName, string lastName, int age)
        {
            this.firstName = firstName;
            this.lastName = lastName;
            this.age = age;
        }
        // parameterised Constructor
        public Human(string firstName, string lastName, string eyeColor, int age)
        {
            this.firstName = firstName;
            this.lastName = lastName;
            this.eyeColor = eyeColor;
            this.age = age;
        }
        // member methos
        public void IntroduceMyself() {
            if (firstName != null && lastName != null && eyeColor != null && age != 0) {
                Console.WriteLine("Hi, I'm {0} {1} and {2} years old. My eye color is {3} ", firstName, lastName, age, eyeColor);
            }
            else if (firstName != null && lastName != null && eyeColor != null && age == 0)
            {
                Console.WriteLine("Hi, I'm {0} {1}. My eye color is {3} ", firstName, lastName, age, eyeColor);
            }
            else if (firstName != null && lastName != null && eyeColor != null && age == 1)
            {
                Console.WriteLine("Hi, I'm {0} {1} and {2} year old. My eye color is {3} ", firstName, lastName, age, eyeColor);
            }
            else if (firstName != null && lastName != null && eyeColor == null && age != 0)
            {
                Console.WriteLine("Hi, I'm {0} {1} and {2} year old.", firstName, lastName, age);
            }
            else if (firstName != null && lastName != null && eyeColor == null && age == 0)
            {
                Console.WriteLine("Hi, I'm {0} {1}.", firstName, lastName);
            }
            else if (firstName != null && lastName == null && eyeColor == null && age == 0)
            {
                Console.WriteLine("Hi, I'm {0}.", firstName);
            }
            else if (firstName == null && lastName == null && eyeColor == null && age == 0)
            {
                Console.WriteLine("Hi, I'm basic.");
            }
            else
            {
                Console.WriteLine("Hi, this shouldn't happen to {0}.", firstName);
            }
            
        }

    }
}
