﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace HelloWorld
{

    // Challenge : create a read-only property "FrontSurface" which calculates the
    // front surface based on height and length
    class Box
    {

        //member variables
        private int length = 3;
        private int height;
        //public int width;
        //private int volume;

        // properties
        public int Width { get; set; }
        
        /*
        public int Width {
            get {
                return this.width;
            }
            set {
                this.width = value;
            }
        
        }
        */

        public int Height {
            get {
                return this.height;
            }
            set {
                if (value < 0)
                {
                    height = -value;
                }
                else
                {
                    height = value;
                }
                
            }
        }

        public Box(int length, int height, int width ) {
            this.length = length;
            this.height = height;
            Width = width;
        }

        public int Volume {
            get {
                return this.Height * this.GetLength() * this.Width;
            }
        }

        public void SetLength(int length) {
            if (length <0)
            {
                throw new Exception("Length should be greater or equal to 0");
            }
            else
            {
                this.length = length;
            }
            
        }

        public int GetLength() {
            return this.length;
        }

        public int FrontSurface{get { return Height * this.length;}}

        public void DisplayInfo() {
            Console.WriteLine("length is {0} and height is {1} and width is {2} so the volume is {3}", 
                length, Height, Width, length*Height*Width);
        }
    }
}
