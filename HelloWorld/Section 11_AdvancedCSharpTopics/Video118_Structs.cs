﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HelloWorld.Section_11_AdvancedCSharpTopics
{
    class Video118_Structs
    {
        public static void Video118_Main() {
            Video118_Game game1;
            game1.name = "POkemon Go";
            game1.developer = "Niatic";
            game1.rating = 3.5;
            game1.releaseDate = "01.07.2016";

            Console.WriteLine("Game 1's name is {0}", game1.name);
            Console.WriteLine("Game 1's developer is {0}", game1.developer);
            Console.WriteLine("Game 1 was relased on {0}", game1.releaseDate);
        }

    }

    struct Video118_Game {
        public string name;
        public string developer;
        public double rating;
        public string releaseDate;

        public Video118_Game(string name, string developer, double rating, string releaseDate) {
            this.name = name;
            this.developer = developer;
            this.rating = rating;
            this.releaseDate = releaseDate;
        }

        public void Display() {
            Console.WriteLine("Game 1's name is {0}", name);
            Console.WriteLine("Game 1's developer is {0}", developer);
            Console.WriteLine("Game 1's rating is {0}", rating);
            Console.WriteLine("Game 1 was relased on {0}", releaseDate);
        }
    }
}
