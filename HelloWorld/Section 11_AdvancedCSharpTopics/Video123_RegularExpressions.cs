﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace HelloWorld.Section_11_AdvancedCSharpTopics
{
    class Video123_RegularExpressions
    {
        public static void Video123_Main() {
            string pattern = @"\d";
            //string pattern = @"\d{5}";
            //string pattern = @"there";
            Regex regex = new Regex(pattern);

            string text = "Hi there, my number is 12314";

            MatchCollection matchcolection = regex.Matches(text);

            Console.WriteLine("{0} hits found: \n {1}", matchcolection.Count, text);
            foreach (Match hit in matchcolection)
            {
                GroupCollection group = hit.Groups;
                Console.WriteLine("{0} found at {1}", group[0].Value, group[0].Index);
            }
        }
    }
}
