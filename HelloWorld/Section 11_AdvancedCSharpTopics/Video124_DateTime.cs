﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HelloWorld.Section_11_AdvancedCSharpTopics
{
    class Video124_DateTime
    {
        public static void Video124_Main() {
            DateTime dateofBirth = new DateTime(1982, 8, 28, 8, 30, 0);
            DateTime dateTime = new DateTime(2018, 5, 31);
            DateTime now = DateTime.Now;

            Console.WriteLine("My birthday is {0}", dateofBirth);
            Console.WriteLine("Now is {0}", now);

            //Console.WriteLine("I have lived {0} days", time);

            // Write today on screen
            Console.WriteLine(DateTime.Today);
            // Write current time on screen
            Console.WriteLine(DateTime.Now);

            // Write on the console which day of week we have
            Console.WriteLine("Today is {0}, which is the {1} days of the month", DateTime.Now.DayOfWeek, DateTime.Now.Day);

            DateTime tomorrow = GetTomorrow();
            Console.WriteLine("Tomorrow is {0}" , tomorrow);

            Console.WriteLine(GetFirstDayOfYear(1999));

            int days = DateTime.DaysInMonth(2000, 2);
            Console.WriteLine("Days in Feb 2000: {0}", days);

            days = DateTime.DaysInMonth(2001, 2);
            Console.WriteLine("Days in Feb 2001: {0}", days);
            days = DateTime.DaysInMonth(2004, 2);
            Console.WriteLine("Days in Feb 2004: {0}", days);


            Console.WriteLine("Minute is {0}", now.Minute);

            //Display the time in this structure
            // X o'Clock and y minutes and z seconds

            Console.WriteLine("{0} o'clock and {1} minutes and {2} seconds", now.Hour, now.Minute, now.Second);

            /*
            Console.WriteLine("Write a date in this format: yyyy-mm-dd");
            string input = Console.ReadLine();
            if (DateTime.TryParse(input, out dateTime))
            {
                Console.WriteLine(dateTime);
                TimeSpan daysPassed = now.Subtract(dateTime);
                Console.WriteLine("Days passed since : {0} days", daysPassed.Days);
            }
            else
            {
                Console.WriteLine("Wrong input");
            }
            */
            DateTime dateOfMarriage = new DateTime(2013, 8, 28);
            DateTime dateOfDavidsBirth = new DateTime(2015, 4, 11);
            DateTime dateOfAdamsBirth = new DateTime(2019, 6, 7);

            TimeSpan daysPassed = dateOfMarriage.Subtract(dateofBirth);
            Console.WriteLine("I was {0} days old on my marriage date", daysPassed.Days);
            daysPassed = now.Subtract(dateOfMarriage);
            Console.WriteLine("Days passed since marriage : {0} days", daysPassed.Days);
            daysPassed = now.Subtract(dateOfDavidsBirth);

            daysPassed = dateOfDavidsBirth.Subtract(dateofBirth);
            Console.WriteLine("I was {0} days old on Davids date of Birth", daysPassed.Days);
            daysPassed = now.Subtract(dateOfDavidsBirth);
            Console.WriteLine("Days passed since Davids Birth : {0} days", daysPassed.Days);

            daysPassed = dateOfAdamsBirth.Subtract(dateofBirth);
            Console.WriteLine("I was {0} days old on Adams date of Birth", daysPassed.Days);
            daysPassed = now.Subtract(dateOfAdamsBirth);
            Console.WriteLine("Days passed since Adams Birth : {0} days", daysPassed.Days);

        }

        static DateTime GetTomorrow() {
            return DateTime.Today.AddDays(1);
        }
        
        static DateTime GetFirstDayOfYear(int year) {
            return new DateTime(year, 1, 1);
        }
    }


}
