﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HelloWorld.Section_11_AdvancedCSharpTopics
{
    enum Video119_Days { Mo, Tu, We, Th, Fr, Sa, Su};
    enum Video119_Month { Jan = 1 , Feb, Mar, Apr, May, Jun, Jul = 12, Aug, Sep, Oct, Nov, Dec };
    class Video119_Enums
    {
        public static void Video119_Main() {
            Video119_Days fr = Video119_Days.Fr;
            Video119_Days su = Video119_Days.Su;

            Video119_Days a = Video119_Days.Fr;

            Console.WriteLine(fr==a);
            Console.WriteLine(Video119_Days.Mo);
            Console.WriteLine((int)Video119_Days.Mo);

            Console.WriteLine(Video119_Month.Jan);
            Console.WriteLine((int)Video119_Month.Jan);

            Console.WriteLine(Video119_Month.Feb);
            Console.WriteLine((int)Video119_Month.Feb);

            Console.WriteLine(Video119_Month.Aug);
            Console.WriteLine((int)Video119_Month.Aug);


            Console.Read();
        }

    }
}
