﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace HelloWorld.Section_11_AdvancedCSharpTopics
{
    class Video121_RandomClass
    {
        public static void Video121_Main() {
            Random dice = new Random();

            int numEyes;

            for (int i = 0; i < 10; i++)
            {
                numEyes = dice.Next(1,7); // exclusive upper value (values between 1 and 6)
                Console.WriteLine(numEyes);
            }

            do
            {
                Console.WriteLine("Ask your question (Press x to exit):");
                string input = Console.ReadLine();
                string response = String.Empty;
                if (input.Equals("x") || input.Equals("X"))
                {
                    break;
                }
                else
                {
                    int responseInt = dice.Next(1, 4);
                    switch (responseInt)
                    {
                        case 1:
                            response = "Yes";
                            break;
                        case 2:
                            response = "Maybe";
                            break;
                        case 3:
                            response = "No";
                            break;
                        default:
                            break;
                    }
                    Console.WriteLine(response);
                }
            } while (true);
            
            
        }
    }
}
