﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HelloWorld.Section_8_Debugging
{
    class Video97_Locals_and_Autos
    {
        public static void LocalsAndAutos()
        {
            List<string> friends = new List<string> { "Frank", "Joe", "Michelle", "Andy", "Maria", "Carlos", "Angelina" };
            List<string> partyFriends = GetPartyFriends(friends, 3);

            foreach (string name in partyFriends)
            {
                Console.WriteLine(name);
            }
        }

        private static List<string> GetPartyFriends(List<string> list, int count)
        {
            List<string> partyFriends = new List<string>();

            while (partyFriends.Count < count)
            {
                var currentFriend = GetPartyFriend(list);
                partyFriends.Add(currentFriend);
                list.Remove(currentFriend);
            }
            return partyFriends;
        }

        private static string GetPartyFriend(List<string> list)
        {
            string shortestName = list[0];
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Length < shortestName.Length)
                {
                    shortestName = list[i];
                }
            }
            return shortestName;
        }
    }
}
