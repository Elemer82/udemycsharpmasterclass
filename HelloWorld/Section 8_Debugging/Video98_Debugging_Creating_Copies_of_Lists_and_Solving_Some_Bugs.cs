﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HelloWorld.Section_8_Debugging
{
    class Video98_Debugging_Creating_Copies_of_Lists_and_Solving_Some_Bugs
    {

        public static void DebuggingCreatingCopiesOfListsAndSolvingSomeBugs()
        {

            List<string> friends = new List<string> { "Frank", "Joe", "Michelle", "Andy", "Maria", "Carlos", "Angelina" };
            List<string> partyFriends = GetPartyFriends(friends, 10);

            Console.WriteLine("Friends are:");
            foreach (string name in friends)
            {
                Console.WriteLine(name);
            }

            Console.WriteLine("PartyFriends are:");
            foreach (string name in partyFriends)
            {
                Console.WriteLine(name);
            }
        }

        private static List<string> GetPartyFriends(List<string> list, int count)
        {
            List<string> buffer = new List<string>(list);
            List<string> partyFriends = new List<string>();

            while (partyFriends.Count < count)
            {
                var currentFriend = GetPartyFriend(buffer);
                partyFriends.Add(currentFriend);
                buffer.Remove(currentFriend);
            }
            return partyFriends;
        }
        /// <summary>
        /// This is the logic to figure out  who is currently shortest name friend
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        private static string GetPartyFriend(List<string> list)
        {
            string shortestName = list[0];
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Length < shortestName.Length)
                {
                    shortestName = list[i];
                }
            }
            return shortestName;
        }
    }
}
